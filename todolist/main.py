from urllib import request
import uvicorn
from fastapi import FastAPI, Request, Depends
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from fastapi.encoders import jsonable_encoder
from todolist.db import SessionLocal, engine, DBContext
from todolist import models
from sqlalchemy.orm import Session

app = FastAPI()
templates = Jinja2Templates(directory="todolist/templates")
app.mount("/static", StaticFiles(directory="todolist/static"), name="static")


def get_db():
    with DBContext() as db:
        yield db


def start():
    """Launched with `poetry run start` at root level"""
    uvicorn.run("todolist.main:app", host="0.0.0.0", port=8000, reload=True)


@app.get("/")
def root(request: Request):
    return templates.TemplateResponse(
        "index.html", {"request": request, "title": "Home"}
    )


@app.get("/tasks")
def get_tasks(db: Session = Depends(get_db)):
    return jsonable_encoder(db.query(models.Task).first())
